# NLP Project - Intelligent News Summarizer 

Full stack project in Flask Python.

## About

This is a demo AI project for educational purposes and learning. The full stack application is developed using
python, Flask and Bootstrap.

### You can choose different algorithms Sumy, Gensim, Term-frequency and LSTM

<img src="git-images/1.png" width="400">!

### Enter the link or the text to be summarized

<img src="git-images/2.png" width="400">

### Get the output and hashtags to post to social media
<img src="git-images/3.png" width="400">

### Post it to the twitter or instagram

<img src="git-images/4.png" width="400">


Checkout the live video walkthrough of the project : 

[link to live project!](https://www.youtube.com/watch?v=82zPFEKhB5I&ab_channel=StatsMatter)