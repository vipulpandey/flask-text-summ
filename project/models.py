import datetime
from project import db

# Getting the data in the database 

class Article(db.Model):
    
    # create the database and the database table
 
    __tablename__ = "articles"
     
    
    Id = db.Column(db.Integer, primary_key = True)
    date = db.Column(db.DateTime)
    created = db.Column(db.DateTime) 
    url = db.Column(db.String(), default = "https://www.statsmatter.in") 
    article_heading = db.Column(db.String,unique=True, nullable=False)
    article_content = db.Column(db.String, nullable=False)
    posted = db.Column(db.Boolean, default=False, nullable=False)

    def __init__(self,date,  created,url, article_heading,article_content, posted):

        self.date = date
        self.created = created        
        self.url = url
        self.article_heading = article_heading
        self.article_content = article_content             
        self.posted = posted

    def __repr__(self):
        return '<title {}'.format(self.article_heading)


# class Processed_Article(db.Model):
    
#     # create the database and the database table
 
#     __tablename__ = "articles"
     
#     # final_sentence_count = db.Column(db.Integer, nullable=False)
#     Id = db.Column(db.Integer, primary_key = True)
#     date = db.Column(db.Date)
#     created = db.Column(db.DateTime)  
#     article_heading = db.Column(db.String,unique=True, nullable=False)
#     article_content = db.Column(db.String, nullable=False)
#     article_content_op = db.Column(db.String, nullable=False)    

#     def __init__(self,date,  created, article_heading,article_content, article_content_op):

#         self.date = date
#         self.created = created        
#         self.article_heading = article_heading
#         self.article_content = article_content
#         self.article_content_op = article_content_op       

#     def __repr__(self):
#         return '<title {}'.format(self.name)