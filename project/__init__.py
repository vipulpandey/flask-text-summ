from flask import Flask 
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect


# config flask app
app = Flask(__name__, static_url_path='/static', instance_relative_config = True)
app.config.from_pyfile('flask.cfg')
db = SQLAlchemy(app)
# blueprints

csrf = CSRFProtect(app)



####################
#### blueprints ####
####################
from project.users.views import users_blueprint
from project.articles.views import articles_blueprint

# register the blueprints
app.register_blueprint(users_blueprint)
app.register_blueprint(articles_blueprint)
