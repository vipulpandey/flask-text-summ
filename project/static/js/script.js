
$(document).ready(function() {
$("#formSumyLexRank").hide();
    $("#formWfreq").hide();
    $("#formGensim").hide();
    // $("#formDl").hide();

    $('body').click(function (event) {

      var log = $('#log');

      if ($(event.target).is('#opSumy')) {
        log.html(event.target.id + ' was clicked.');
        $("#formSumyLexRank").show();

        // hide the other forms
        $("#formWfreq").hide();
        $("#formGensim").hide();

      } else if ($(event.target).is('#opWfreq')) {
        log.html(event.target.id + ' was clicked.');
        $("#formWfreq").show();

        // hide the other forms
        $("#formSumyLexRank").hide();
        $("#formGensim").hide();

      } else if ($(event.target).is('#opGensim')) {
        log.html(event.target.id + ' was clicked.');
        $("#formGensim").show();
        // hide the other forms
        $("#formSumyLexRank").hide();
        $("#formWfreq").hide();

      } else if ($(event.target).is('#opDl')) {
        log.html(event.target.id + ' was clicked.');
      } else {
        log.html('');
      }
    });


});


$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
    var myString        = `<div>
    <div class="form-group">
    <div class="form-group row shadow-textarea">
        <div class="col-xs-4">
            <label for="videoCreationTextForm">Headline</label>
            <textarea class="form-control z-depth-1" id="exampleFormControlTextarea7" name="video_title" rows="2"
              placeholder="Write something here..."></textarea>
        </div>
        <div class="col-xs-6">
            <label for="videoContenTextForm">Content</label>
            <textarea class="form-control z-depth-1" id="exampleFormControlTextarea8" name="video_description" rows="4"
              placeholder="Write something here..."></textarea>
        </div>
    </div> 
     </div>
     <a href="#" class="remove_field">Remove</a>
     </div>
    
    `;
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
            $(wrapper).append(myString); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});

