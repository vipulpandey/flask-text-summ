import numpy as np
import cv2
import glob
import os, shutil
import re
from project import app
from . import graphic_image_manipulation
from . import graphic_text_manipulation


def _split_words_to_letters(word):
    return list(word) 
   
# graphic_image_manipulation._add_blend_images()


def _get_the_background_image(height, width):
    # background h,w
    height, width = width,height
    #create a 512x512 black image
    # img=np.zeros((height,width,3),np.uint8)

    img=np.ones((height,width,3),np.uint8)

    # pick other colors for background    
    # img[:,0:width//2] = (232,225,164)  #blue
    # img[:,width//2:width] = (255,255,255) #white

    return img

def _get_size_of_letter(letter):

    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1.5
    margin = 5
    thickness = 2
    color = (0,0,255)
    size = cv2.getTextSize(letter, font, font_scale, thickness)
    w, h = size[0]
    return size[0]


def convert_images_to_videos(path_to_input_images):

    # for filename in sorted(glob.glob('op_videos/*.jpg')):
    img_array = []
    for filename in path_to_input_images:
        
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        
        img_array.append(img)

    # write in mp4 format        
    _civ_frame_rate = 18
    print("-----path to videos -----------------")
    print(app.config['UPLOAD_FOLDER'] + 'videos/project.mp4')
    out = cv2.VideoWriter(app.config['UPLOAD_FOLDER'] + 'videos/project.mp4',0x7634706d, _civ_frame_rate, size, True)
    # out = cv2.VideoWriter('project.mp4',0x00000021, 10, size)
    
    for i in range(len(img_array)):
        # print(path_to_input_images[i])
        out.write(img_array[i])
    out.release()



def _get_text_to_screen_mapping(screen_width, screen_height, text):

    '''
        input: gets text
        output : gives dimensions of output texts
    '''
    # split text into words
    wordLength_tuple = [(w, _get_size_of_letter(w)) for w in text.split()]
    # print(wordLength_tuple)    

    horizontal_padding, vertical_padding = 20, 40
    num_of_rows_possible = screen_height//22
    
    possible_words_in_line_lists = []
    l = 1
    word_length, word_padding = 0, 50
    # start temp with dummy words
    temp = []
    for each in wordLength_tuple:
        w = (each[0])
        # print(w)
        # check if comination of words is greater than line length
        
        if(len(wordLength_tuple) < len(temp)):
            # print("different reality1")
            break
        ########## in case words doesn't fit in the line increase word_padding
        elif (word_length+ word_padding < screen_width  ):
            word_length = word_length + each[1][0] + word_padding*3
            temp.append(each[0])
            # print("different reality2")
            # print(word_length,temp)
        else:
            # print(word_length,temp)
            temp.append(each[0])
            # append word lists to parent
            possible_words_in_line_lists.append(temp)
            # start a new line reset temp lists
            # print("different reality3")
            temp = []
            word_length = 0                

        l = l + 1

    possible_words_in_line_lists.append(temp)

    return possible_words_in_line_lists


# all_possible_words_in_line_lists = _get_text_to_screen_mapping(500, 400, para)
# print("------hyie----")
# print(all_possible_words_in_line_lists)


def get_images_from_text( form):

    title = form.video_title.data
    text = form.video_description.data
    print(title, text)
    print(app.config['UPLOAD_FOLDER'])
    ## initializearray of images to create videos    
    path_to_input_images = []           
    
    # background info
    # h,w = 512,720
    h,w = 1280,720
    img  = _get_the_background_image(1280,720)

    ######## ADDING IMAGES BEFORE CONTENT

    path_to_seed_image = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(0)+'.jpg'    
    path_to_intro_images = app.config['UPLOAD_FOLDER'] + 'videos/op_videos/intro.jpg'    
    cv2.imwrite( path_to_seed_image, img)
    
    path_first_image_to_be_added = app.config['UPLOAD_FOLDER'] + 'videos/input-video-creation-images/slide4.jpg'
    
    path_to_resized_image = graphic_image_manipulation._resize_to_specific_size((1280,720), path_first_image_to_be_added)
    print(path_to_resized_image)
    ################# add images related operations
    # path_to_final_image = graphic_image_manipulation._add_blend_images(path_to_seed_image, path_first_image_to_be_added )
    # path_to_final_image_array = cv2.imread( path_to_final_image, cv2.IMREAD_UNCHANGED)
    # print("testing ---------"); print(path_to_intro_images); print(path_to_final_image_array)
    # cv2.imwrite( path_to_intro_images, path_to_final_image_array)
    
    
    #######-- decide how long intro image should be shown
    
    # intro_image_frame_rate = 40
    # # path_to_input_images = [path_to_intro_images for i in range(intro_image_frame_rate)]
    # path_to_input_images = [path_to_resized_image for i in range(intro_image_frame_rate)]
    # print(path_to_input_images)

    ### ADDING SECOND IMAGE AFTER INTRO
    path_to_intro1_images = app.config['UPLOAD_FOLDER'] + 'videos/op_videos/intro1.jpg' # placeholder
    path_second_intro_to_be_added = app.config['UPLOAD_FOLDER'] + 'videos/input-video-creation-images/slide1.jpg'
    path_to_second_intro = graphic_image_manipulation._add_blend_images(path_to_seed_image, path_second_intro_to_be_added )
    path_to_second_intro_array = cv2.imread( path_to_second_intro, cv2.IMREAD_UNCHANGED)
    cv2.imwrite( path_to_intro1_images, path_to_second_intro_array )

    ###### decide how long the second intro will be shown

    # for i in range(intro_image_frame_rate):
    #     path_to_input_images.append(path_to_intro1_images)    

    ############ putting header text on screen ###############

    # path_to_seed_image = 'op_videos/'+str()+'.jpg' 
    # yellow color color':(3, 186, 252) blue (255, 204, 51)
    
    style_Dict1 = {'thickness': 1, 'color':(255, 255, 255) , 'font_scale':0.8  }
    style_Dict2 = {'thickness': 2, 'color':(255, 80, 255) , 'font_scale':1.5  }
    h_Text = 'By StatsMatter - @statsmattersm ( Dec-2019)'
    # h_Text = 'Indian AI Startups By StatsMatter - @statsmattersm'
    gap_bw_Headers = 20
    header_Text1 = h_Text
    header_Text2 =  form.video_title.data
    header_w, header_h = _get_size_of_letter(header_Text2);
    header_w2, header_h2 = _get_size_of_letter(header_Text2)
    header_Pos_x = int(1280/2-header_w/2)  ; header_Pos_y = 40 ;
    header_Pos_x2 = int(1280/2-header_w2/2*1.2)  ; header_Pos_y2 = 40 ;
    header_Pos1 = (header_Pos_x,header_Pos_y) ; header_Pos2 = (header_Pos_x,header_Pos_y + int(header_h )+gap_bw_Headers); 
    text_info_Dict1 = {'txt': header_Text1, 'path': path_to_seed_image, 'pos': header_Pos1,'style' : style_Dict1  }
    text_info_Dict2 = {'txt': header_Text2, 'path': path_to_seed_image, 'pos': header_Pos2,'style' : style_Dict2  }
    graphic_text_manipulation.put_header_text_on_screen(text_info_Dict1)
    graphic_text_manipulation.put_header_text_on_screen(text_info_Dict2)
   
    # get how text will look on screen
    wordsLine_lists = _get_text_to_screen_mapping(h,w,text)
    print(wordsLine_lists)
    # get x and y position to start with
        
    # x_pad_initial, y_pad = 30, 130
    x_pad_initial, y_pad = 20, 100

    # style for letters
    font = cv2.FONT_HERSHEY_DUPLEX
    font_scale = 1.5
    thickness = 2
    # color = (51, 51, 0)
    color = (255,255,255)
    # color = (255, 255, 102)
    size = cv2.getTextSize(text, font, font_scale, thickness) 

    #now use a frame to show it just as displaying a image
    num = 1
    
    # writing letters on images
    for word_list in wordsLine_lists:
        # set the position of start point of video
        x_pad = x_pad_initial
        y_pad  = y_pad + 55
        # print(x_pad, y_pad)
        padding_bw_letters = 0

        for word in word_list:
            # add space after the word & split word to letters
            word = word + " "
            text_letters = _split_words_to_letters(word)

            for letter in text_letters:            
                
                # read previous image
                img = cv2.imread(app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(num - 1) + '.jpg')
                w, h = _get_size_of_letter(letter)

                # print(letter,w)
                cv2.putText(img, letter,(x_pad+padding_bw_letters,y_pad), font, font_scale,color,thickness,cv2.LINE_AA)
                cv2.imwrite(app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(num)+'.jpg',img)
                x_pad = x_pad + w 
                path_to_input_images.append(app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(num)+'.jpg')
                
                num = num + 1

    # show the last frame a bit longer
    
    for i in range(20):        
        path_to_input_images.append(path_to_input_images[-1])

    # convert to video
    convert_images_to_videos( path_to_input_images )

# text = '''
# Founded in 2014 by Shashank Kumar and Harshil Mathur in 2014, Razorpay is a payments platform, which claims 
# to have over 350K clients (businesses), who are using its service. Some of its clients include IRCTC, Airtel,
# BookMyShow, Zomato, Swiggy, Yatra and Zerodha, among others. Earlier in 2018, Razorpay launched Razorpay Capital,
# marking its entry into the lending space.

# '''

# slowdown_videos()
####################################################################################
# get_images_from_text(text)

# cv2.waitKey(0)           
# cv2.destroyAllWindows()

def get_future_video_creation_info(riv_path_to_ip_images_folder,per_image_fps ):
    '''
    ** gives the approximate length and special effects info dictionary
    '''
    

def get_Video_With_Effects(gve_ip_images_path_list, gve_video_effect_metadata):
    '''
        input : list of images on which effect needs to be applied
        output : path of Video in mp4 format
    '''
    print("---------entered  get_Video_With_Effects")
    print(gve_ip_images_path_list)
    print(gve_video_effect_metadata['gve_whole_video_effect'])
    
    if gve_video_effect_metadata['gve_whole_video_effect']:
        print("apply effect all")
        
    else:        
        print("no effects")


def repeat_images_for_video(riv_path_to_ip_images_folder, per_image_fps):

    '''
    '''
    print("----------repeat images")
    # pick all the images and output it it in the next folder with frames
    riv_num_of_ip_images = glob.glob(riv_path_to_ip_images_folder + '/' +"*.png")  # input images folder
    riv_path_to_op_images_folder = app.config['UPLOAD_FOLDER'] + 'videos/temp1'    #output images folder
    print(riv_num_of_ip_images)
    
    i = 0 ; j = 0;
    for img in range(len(riv_num_of_ip_images)):
        img_path = riv_path_to_ip_images_folder + '/' + str(i) + '.png'
        print(j)
        # check if effect has to be applied
        
        # copy ip images to op images folder frame_rate times
        for num in range(per_image_fps):            
            shutil.copyfile(img_path, riv_path_to_op_images_folder + '/' + str(j) + '.png')
            j = j + 1
        
        i = i + 1

    
    return riv_path_to_op_images_folder


def repeat_images_fps_video(riv_path_to_ip_images_folder, per_image_fps):

    '''
    '''
    print("----------repeat images")
    # pick all the images and output it it in the next folder with frames
    riv_num_of_ip_images = glob.glob(riv_path_to_ip_images_folder + '/' +"*.png")  # input images folder
    riv_path_to_op_images_folder = app.config['UPLOAD_FOLDER'] + 'videos/temp1'    #output images folder
    print(riv_num_of_ip_images)
    
    i = 0 ; j = 0;
    for img in range(len(riv_num_of_ip_images)):
        img_path = app.config['UPLOAD_FOLDER'] + 'videos/temp/' + str(i) + '.png'
        
        # check if effect has to be applied
        
        # copy ip images to op images folder frame_rate times
        for num in range(per_image_fps):            
            shutil.copyfile(img_path, riv_path_to_op_images_folder + '/' + str(j) + '.png')
            j = j + 1
        
        i = i + 1

    
    return riv_path_to_op_images_folder