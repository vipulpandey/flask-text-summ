import numpy as np
import cv2
import glob
import re
import os, shutil
from project import app
from . import graphic_image_manipulation
from . import graphic_text_manipulation
from . import gully_news_video_template
from . import create_video
from PIL import Image, ImageDraw, ImageFont
from moviepy.editor import *

def _split_words_to_letters(word):
    return list(word) 
   
# graphic_image_manipulation._add_blend_images()


def _get_the_background_image(height, width):
    # background h,w
    height, width = width,height
    #create a 512x512 black image
    # img=np.zeros((height,width,3),np.uint8)

    img=np.ones((height,width,3),np.uint8)

    # pick other colors for background    
    # img[:,0:width//2] = (232,225,164)  #blue
    # img[:,width//2:width] = (255,255,255) #white

    return img

def get_video_effect_image_list(get_ip_image_list, num_split):

    return(np.array_split(get_ip_image_list,num_split))


def convert_images_to_videos(path_to_input_images):

    '''
        cv2 video writer
        input : folder of images to be converted to video
        output : path of video file
    '''
    print("---------entered convert images to videos")    
    img_array = []
    path_to_input_images_array = sorted(glob.glob(path_to_input_images + '/' +"*.png"))
    i = 0
    for filename in range(len(path_to_input_images_array)):        
        img = cv2.imread(path_to_input_images + '/'+str(i)+'.png')
        height, width, layers = img.shape
        size = (width,height)
        
        img_array.append(img)
        i = i + 1
    # write in mp4 format        
    _civ_frame_rate = 18
    print("-----path to videos -----------------")
    
    out = cv2.VideoWriter(app.config['UPLOAD_FOLDER'] + 'videos/project.mp4',0x7634706d, _civ_frame_rate, size, True)
    # out = cv2.VideoWriter('project.mp4',0x00000021, 10, size)
    
    for i in range(len(img_array)):
        
        out.write(img_array[i])
    out.release()
    cv2.destroyAllWindows()
    
    # delete variables
    del img_array
    video_output_path = app.config['UPLOAD_FOLDER'] + 'videos/project.mp4'
    

    return video_output_path


# all_possible_words_in_line_lists = _get_text_to_screen_mapping(500, 400, para)
# print("------hyie----")
# print(all_possible_words_in_line_lists)

def preprocess_images_for_video():
    '''
        create images to be joined for video
    '''

def put_text_on_slide_with_background(ptsb_slide_location,  words_fit_inline_list, pts_text_pos, pts_text_design,pts_text_style, CONTINUE_ON_NEXT_SLIDE = False):
    '''
      input :   takes the text style as input
      function : puts text on the base slide on given position
      returns : text list 
    '''

    print("------------------put_text_on_slide start")
    

    

    print("----------put text with background")
    print(words_fit_inline_list)    

    
    #####################
    ################### VIDEO CREATION
    ######################

    # setup seed slide for the video
    slides_counter = 0
    
    CURRENT_SLIDE_PATH  = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp/'+str(slides_counter)+'.png'  # png

    # create Image object with the input image        
    image = Image.open(ptsb_slide_location)

    # size() returns a tuple of (width, height) 
    image_size = image.size 

    (SCREEN_W, SCREEN_H) = (image_size[0], image_size[1])
    # close image pillow context
    image.close()                               


    image = Image.open(CURRENT_SLIDE_PATH)                   
    draw = ImageDraw.Draw(image)

    # check the line padding along y-axis
    BETWEEN_LINE_PADDING =  pts_text_design["td_font"].getsize('hg')[1]
    (x_pos,y_pos) = (pts_text_pos[0], pts_text_pos[1])
        
    i = 0
    
    while( i < (len(words_fit_inline_list) - 1) ):      


        if(y_pos < SCREEN_H - BETWEEN_LINE_PADDING ):
                        
            # draw it on the image
            # if backgound images needs to b added 
            
            draw.text((x_pos,y_pos) , words_fit_inline_list[i], fill=pts_text_design['td_color'], font= pts_text_design['td_font'])
            y_pos = y_pos + BETWEEN_LINE_PADDING
             
            # saving the image location
            
            image.save(CURRENT_SLIDE_PATH, optimize=True, quality=100)            
        
        else:
            # close the last image
            image.close()                                   
            slides_counter = slides_counter + 1

            # setup up new bacground image
            
            CURRENT_SLIDE_PATH = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp/'+str(slides_counter)+'.png'           

            # create Image object with the input image  
                
            image = Image.open(CURRENT_SLIDE_PATH)           

            # initialise the drawing context with  -- the image object as background        
            draw = ImageDraw.Draw(image)

            # reset y position
            y_pos = pts_text_pos[1]
            # go back two sentences, current one and last missed sentence
            i = i - 1
            

        i = i + 1         ## while loop ends

    
    # create videos out of image

    # show thumbnails on front end

    return True



# def put_text_on_slide(pts_slide_location,  pts_text, pts_text_pos, pts_text_design,pts_text_style, CONTINUE_ON_NEXT_SLIDE = False):
#     '''
#       input :   takes the text style as input
#       function : puts text on the base slide on given position
#       returns : text list 
#     '''

#     print("------------------put_text_on_slide start")
#     # create Image object with the input image        
#     image = Image.open(pts_slide_location)

#     # size() returns a tuple of (width, height) 
#     image_size = image.size 

#     # initialise the drawing context with  -- the image object as background        
#     draw = ImageDraw.Draw(image)

#     ############### input string cleaning

#     pts_text_final = " ".join(pts_text.split())
    
#     ## Get total text length
#     TOTAL_TEXT_LENGTH = pts_text_design["td_font"].getsize(pts_text_final )[0]    

#     ## split the sentences into words
#     _pts_word_list = pts_text_final.split(" ")    
#     TOTAL_TEXT_WORDS_COUNT = len(_pts_word_list)
#     # add one more word for sentence padding - helpful for printing last word and sentence on slides
#     _pts_word_list.append(" " )

#     word_index = 0
#     combo_words = ""
#     words_fit_inline_list = []
#     (SCREEN_W, SCREEN_H) = (image_size[0], image_size[1])
#     # total_text_length - rolling_text_length < screen
#     rolling_text_length = 0
#     rolling_text_words_count = 0
#     remaining_words = ""

#     for word_index in range(len(_pts_word_list)):
        
#         # get current word length
#         CURRENT_WORD_LENGTH = pts_text_design["td_font"].getsize(_pts_word_list[word_index])[0]
        
#         combo_words = combo_words + _pts_word_list[word_index] + " "    # ADD Current word
                
#         combo_words_length = pts_text_design["td_font"].getsize(combo_words)[0]        
#         ### CHeck if combowords plus next word length < screen width
        
#         if(word_index < len(_pts_word_list ) - 1 ):
#             words_not_fit_in_line_condition = (combo_words_length + pts_text_design["td_font"].getsize(_pts_word_list[word_index + 1])[0]) > SCREEN_W - pts_text_pos[0]
#             if(words_not_fit_in_line_condition):
#                 words_fit_inline_list.append(combo_words)
#                 combo_words = ""

#             words_fit_in_line_condition = (combo_words_length + pts_text_design["td_font"].getsize(_pts_word_list[word_index + 1])[0]) < SCREEN_W - pts_text_pos[0]
#             check_last_sentence = (TOTAL_TEXT_LENGTH - rolling_text_length) < SCREEN_W - pts_text_pos[0]            
#             if(words_fit_in_line_condition and check_last_sentence):

#                 remaining_words = remaining_words +  _pts_word_list[word_index] + " "
                
#                 print(remaining_words)
#                 print(rolling_text_words_count)
#         # append last remaining words         
        

#         rolling_text_length = rolling_text_length + pts_text_design["td_font"].getsize(_pts_word_list[word_index] +" ")[0]
#         rolling_text_words_count = rolling_text_words_count + 1


#         print(TOTAL_TEXT_WORDS_COUNT,rolling_text_words_count)

#         word_index = word_index + 1

#     # # adding the last sentence    

#     words_fit_inline_list.append(remaining_words )

#     # add last word
#     words_fit_inline_list.append(_pts_word_list[-1] )
#     print(words_fit_inline_list)    

    
#     #####################
#     ################### VIDEO CREATION
#     ######################

#     # setup seed slide for the video
#     slides_counter = 0
#     # CURRENT_SLIDE_PATH  = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.jpg'  # jpg
#     CURRENT_SLIDE_PATH  = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.png'  # png
#     image.save(CURRENT_SLIDE_PATH, optimize=True, quality=20)
#     # close image pillow context
#     image.close()                               

#     add_images_to_background_flag = False

#     image = Image.open(CURRENT_SLIDE_PATH)                   
#     draw = ImageDraw.Draw(image)

#     # check the line padding along y-axis
#     BETWEEN_LINE_PADDING =  pts_text_design["td_font"].getsize('hg')[1]
#     (x_pos,y_pos) = (pts_text_pos[0], pts_text_pos[1])
        
#     i = 0
    
#     while( i < (len(words_fit_inline_list) - 1) ):      


#         if(y_pos < SCREEN_H - BETWEEN_LINE_PADDING ):
                        
#             # draw it on the image
#             # if backgound images needs to b added 
            
#             draw.text((x_pos,y_pos) , words_fit_inline_list[i], fill=pts_text_design['td_color'], font= pts_text_design['td_font'])
#             y_pos = y_pos + BETWEEN_LINE_PADDING
             
#             # saving the image location
            
#             image.save(CURRENT_SLIDE_PATH, optimize=True, quality=100)            
        
#         else:
#             # close the last image
#             image.close()                                   
#             slides_counter = slides_counter + 1

#             # create Image object with the input image  
                
#             image = Image.open(pts_slide_location)           

#             # initialise the drawing context with  -- the image object as background        
#             draw = ImageDraw.Draw(image)

#             # setup up new bacground image
#             # CURRENT_SLIDE_PATH = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.jpg'
#             CURRENT_SLIDE_PATH = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.png'
#             # image.save(CURRENT_SLIDE_PATH, optimize=True, quality=20)

#             # reset y position
#             y_pos = pts_text_pos[1]
#             # go back two sentences, current one and last missed sentence
#             i = i - 1
            

#         i = i + 1         ## while loop ends

    
#     # create videos out of image

#     # show thumbnails on front end

#     return words_fit_inline_list

def put_text_on_slide(pts_slide_location,  pts_text, pts_text_pos, pts_text_design,pts_text_style, CONTINUE_ON_NEXT_SLIDE = False):
    '''
      input :   takes the text style as input
      function : puts text on the base slide on given position
      returns : text list 
    '''

    print("------------------put_text_on_slide start")
    # create Image object with the input image        
    image = Image.open(pts_slide_location)

    # size() returns a tuple of (width, height) 
    image_size = image.size 

    # initialise the drawing context with  -- the image object as background        
    draw = ImageDraw.Draw(image)

    ############### input string cleaning

    pts_text_final = " ".join(pts_text.split())
    
    ## Get total text length
    TOTAL_TEXT_LENGTH = pts_text_design["td_font"].getsize(pts_text_final )[0]    

    ## split the sentences into words
    _pts_word_list = pts_text_final.split(" ")    
    
    # add one word in the last

    _pts_word_list.append(" ")

    TOTAL_TEXT_WORDS_COUNT = len(_pts_word_list)

    word_index = 0
    combo_words = ""
    words_fit_inline_list = []
    (SCREEN_W, SCREEN_H) = (image_size[0], image_size[1])
    # total_text_length - rolling_text_length < screen
    
    rolling_text_words_count = 0
    remaining_words = ""

    for word_index in range(len(_pts_word_list)):
        
        # get current word length
        CURRENT_WORD_LENGTH = pts_text_design["td_font"].getsize(_pts_word_list[word_index])[0]
        
        combo_words = combo_words + _pts_word_list[word_index] + " "    # ADD Current word
                
        combo_words_length = pts_text_design["td_font"].getsize(combo_words)[0]        
        ### CHeck if combowords plus next word length < screen width
        
        if(word_index < len(_pts_word_list ) - 1 ):
            words_not_fit_in_line_condition = (combo_words_length + pts_text_design["td_font"].getsize(_pts_word_list[word_index + 1])[0]) > SCREEN_W - pts_text_pos[0]
            if(words_not_fit_in_line_condition):
                last_sentence = ""
                words_fit_inline_list.append(combo_words)
                
                rolling_text_words_count = rolling_text_words_count + 1                
                combo_words = ""
            else:                
                
                last_sentence = combo_words
                
        word_index = word_index + 1

    print(TOTAL_TEXT_WORDS_COUNT,rolling_text_words_count)       
    print(last_sentence)
    # add last word
    # words_fit_inline_list.append(_pts_word_list[-1] )
    words_fit_inline_list.append(last_sentence)
    words_fit_inline_list.append(" ")      # add a dummy space as the last sentence
     
    print(words_fit_inline_list) 
    #####################
    ################### VIDEO CREATION
    ######################

    # setup seed slide for the video
    slides_counter = 0
    # CURRENT_SLIDE_PATH  = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.jpg'  # jpg
    CURRENT_SLIDE_PATH  = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.png'  # png
    image.save(CURRENT_SLIDE_PATH, optimize=True, quality=20)
    # close image pillow context
    image.close()                               

    add_images_to_background_flag = False

    image = Image.open(CURRENT_SLIDE_PATH)                   
    draw = ImageDraw.Draw(image)

    # check the line padding along y-axis
    BETWEEN_LINE_PADDING =  pts_text_design["td_font"].getsize('hg')[1]
    (x_pos,y_pos) = (pts_text_pos[0], pts_text_pos[1])
        
    i = 0
    
    while( i < (len(words_fit_inline_list) - 1) ):      


        if(y_pos < SCREEN_H - BETWEEN_LINE_PADDING ):
                        
            # draw it on the image
            # if backgound images needs to b added 
            
            draw.text((x_pos,y_pos) , words_fit_inline_list[i], fill=pts_text_design['td_color'], font= pts_text_design['td_font'])
            y_pos = y_pos + BETWEEN_LINE_PADDING
            
            # print(words_fit_inline_list[i])
            # saving the image location
            
            image.save(CURRENT_SLIDE_PATH, optimize=True, quality=100)            
        
        else:
            # close the last image
            image.close()                                   
            slides_counter = slides_counter + 1

            # create Image object with the input image  
                
            image = Image.open(pts_slide_location)           

            # initialise the drawing context with  -- the image object as background        
            draw = ImageDraw.Draw(image)

            # setup up new bacground image
            # CURRENT_SLIDE_PATH = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.jpg'
            CURRENT_SLIDE_PATH = app.config['UPLOAD_FOLDER'] + 'videos/temp/'+str(slides_counter)+'.png'
            # image.save(CURRENT_SLIDE_PATH, optimize=True, quality=20)

            # reset y position
            y_pos = pts_text_pos[1]
            # go back two sentences, current one and last missed sentence
            i = i - 1
            

        i = i + 1         ## while loop ends

    
    # create videos out of image

    # show thumbnails on front end

    return words_fit_inline_list


def get_video_style_first(form):
    title = form.video_title.data
    text = form.video_description.data
    # print(title, text)
    
    ## initialize array of images to create videos    
    path_to_input_images = []           
    
    # background info
    # h,w = 512,720
    canvas_h,canvas_w = 1280,720
    img  = _get_the_background_image(1280,720)

    ######## ADDING BASE IMAGE 

    # path_to_seed_image = app.config['UPLOAD_FOLDER'] + 'videos/input/'+str(0)+'.jpg'  # for jpg
    path_to_seed_image = app.config['UPLOAD_FOLDER'] + 'videos/output/'+str(0)+'.png'  # for png image        
    
    ########### put text on image
    # desired size
    font_address = app.config['UPLOAD_FOLDER'] + 'fonts/RobotoMono-Medium.ttf'
    font = ImageFont.truetype(font_address, size = 60)
    
    # get the style of text to be written on screen
    print("----checkpoint1----")    
    # starting position of the message
    
    color = 'rgb(255, 255, 255)' # white color
    # color = 'rgb(0, 0, 0)' # green color

    ####### check the text will fit in the slides

    ####### put text on the slide
    # pts_slide_location = app.config['UPLOAD_FOLDER'] + 'videos/temp/0.jpg'
    pts_slide_location = path_to_seed_image
    pts_text_pos = (70,720 - 200) ;
    # pts_text_style = ["normal", "fadein", "fadeout"]
    pts_text_design = {'td_font' : font,'td_color' : color  }
    

    # first check how the slides will look
    put_text_on_slide(pts_slide_location ,pts_text =  text,pts_text_pos =  pts_text_pos,pts_text_design =  pts_text_design, pts_text_style = "normal", CONTINUE_ON_NEXT_SLIDE = False )     # returns : Boolean

    add_image_to_bg_location = app.config['UPLOAD_FOLDER'] + 'videos/output/'+str(0)+'.png'
    
    # temproary graphics sequence with images ---get list of texts to be arranged on slides
    words_fit_inline_list = put_text_on_slide(add_image_to_bg_location ,pts_text =  text,pts_text_pos =  pts_text_pos,pts_text_design =  pts_text_design, pts_text_style = "normal", CONTINUE_ON_NEXT_SLIDE = False )
     
    ######## put text on slides with background

    # create number of slides for the video conversion from the input images
    per_image_fps = 30*3
    riv_path_to_ip_images_folder = app.config['UPLOAD_FOLDER'] + 'videos/temp'

    get_ip_image_list_unordered = glob.glob(riv_path_to_ip_images_folder + '/' +"*.png")

    
    video_effects_img_list = get_video_effect_image_list(get_ip_image_list_unordered, num_split = 6)
    

     # get raw background input images and do preprocessing
    OP_BACKGROUND_IMAGES_PATH =  app.config['UPLOAD_FOLDER'] + 'videos/input/'
    for j in range(len(video_effects_img_list)): 
        op_raw_image =  app.config['UPLOAD_FOLDER'] + 'videos/input/'+str(j)+'.png'      
        ip_raw_image = app.config['UPLOAD_FOLDER'] + 'videos/input-video-creation-images/'+str(j)+'.jpg'
        OP_IMAGE_DIMENSION = (1280,720)
        im = Image.open(ip_raw_image)
        imResize = im.resize(OP_IMAGE_DIMENSION, Image.ANTIALIAS)
        imResize.save(op_raw_image , 'png', quality=90)

    print("---happening")
   
    # prepare initial canvas for the images with backgorund
    temp_folder = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp'
    for k in range(len(get_ip_image_list_unordered)):
        
        temp_img_path  = temp_folder + '/'+ str(k)+'.png'
        
        img_t = _get_the_background_image(canvas_h, canvas_w)
        cv2.imwrite(temp_img_path,img_t)

        # del image
        del img_t
    ########## prepare slides for superiposing the background with the canvas

    get_op_image_list_unordered = glob.glob(temp_folder + '/' +"*.png")    # get all the background images to create partition
    get_op_image_list_ordered = []             # background images in order
    i = 0
    while( i < (len(get_op_image_list_unordered) )):        
        get_op_image_list_ordered.append((temp_folder + '/'+ str(i) + '.png'))

        i = i + 1

    video_effects_img_list2 = get_video_effect_image_list(get_op_image_list_ordered, num_split = 5)  # num_split is the number of background images that will be used

    i_eff = 0
    for img_partition in video_effects_img_list2:
        print("partion---",str(i_eff))                

        # pick image to be added to the background
        on_top_image = app.config['UPLOAD_FOLDER'] + 'videos/input/'+str(i_eff)+'.png'
        for each_image in img_partition:
            
            # pick image 2
            alpha_blended = graphic_image_manipulation.BlendImagesPillow(each_image, on_top_image )
            alpha_blended.save(each_image)
            del alpha_blended

        i_eff = i_eff + 1
    
    del i_eff

    ############ adding logo or text on slides
    pii_input_images_dir = temp_folder
    pii_input_images__list = get_op_image_list_ordered
    pii_image_to_paste_path = app.config['UPLOAD_FOLDER'] + 'videos/logos/gully_news_logo.png'
    
    print(" -------nope not working")
    i_pii = 0
    for pii_img1 in pii_input_images__list:
        print("-----loop started")
        print(pii_img1); print(pii_image_to_paste_path)
        pii_image_op = graphic_image_manipulation.paste_Image_on_Image(pii_img1, pii_image_to_paste_path)
        pii_image_op.save(pii_img1)

        del pii_image_op

        i_pii = i_pii + 1

    # del var 
    del i_pii

    ########### add video end image
    print("---moving file")
    print(str(len(get_op_image_list_unordered)))
    shutil.copy(app.config['UPLOAD_FOLDER'] + 'videos/input-video-creation-images/video_end.png', temp_folder)
    
    n1 = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp/video_end.png'
    n2 = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp/' + str(len(get_op_image_list_unordered))+'.png'
    os.rename(n1, n2)#rename

    ########## write text on slides with background
    ptsb_slide_location = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp/'+str(0) +'.png'
    
    put_text_on_slide_with_background(ptsb_slide_location,  words_fit_inline_list, pts_text_pos, pts_text_design,pts_text_style = "normal", CONTINUE_ON_NEXT_SLIDE = False)

    # create number of slides for the video conversion from the input images
    per_image_fps = 30*4
    riv_path_to_ip_images_folder = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp'

    # repeat images before creating videos
    riv_path_to_op_images_folder = create_video.repeat_images_for_video(riv_path_to_ip_images_folder, per_image_fps)

    # create video
    video_ip_path = convert_images_to_videos(riv_path_to_op_images_folder)
    # video_ip_path = app.config['UPLOAD_FOLDER'] + 'videos/project.mp4'

    # delete temproarya images
    files = glob.glob(app.config['UPLOAD_FOLDER'] + 'videos/temp1'+ '/' +'*.png')
    for f in files:
        os.remove(f)
    audio_ip_path = app.config['UPLOAD_FOLDER'] + 'audio/bensound-endlessmotion.mp3'
    final_video_op_path = app.config['UPLOAD_FOLDER'] + 'videos/output/'
    # add audio to the video
    # print("----------adding audio with video")
    
    # videoclip = VideoFileClip(video_ip_path)
    # print(videoclip.duration)
    # audioclip = AudioFileClip(audio_ip_path)
    # print(audioclip.duration)

    # # new_audioclip = CompositeAudioClip([audioclip])
    # # videoclip.audio = new_audioclip
    # video_with_new_audio  = videoclip.set_audio(audioclip)
    # video_with_new_audio = video_with_new_audio.set_duration(videoclip.duration, change_end=True)
    # video_with_new_audio.write_videofile("new_filename.mp4")

    return True