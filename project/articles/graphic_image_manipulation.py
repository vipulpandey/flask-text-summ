# import OpenCV file 

import cv2 
import os, shutil
import glob
from PIL import Image  # Python Image Library - Image Processing

from project import app

def _resize_image(ri_path1, ri_path2):

    img = cv2.imread(ri_path1, cv2.IMREAD_UNCHANGED)
    img2 = cv2.imread(ri_path2, cv2.IMREAD_UNCHANGED)

    cv2.imwrite(app.config['UPLOAD_FOLDER'] + 'videos/static/temp/temp1.jpg', img)
    cv2.imwrite(app.config['UPLOAD_FOLDER'] + 'videos/static/temp/temp2.jpg', img2)
    scale_percent = 100 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    print(dim)

    # resize image
    resized = cv2.resize(img2, dim, interpolation = cv2.INTER_AREA)
    cv2.imwrite(app.config['UPLOAD_FOLDER'] + 'videos/temp/temp.jpg', resized)
    path_to_upscaled_resized_image = app.config['UPLOAD_FOLDER'] + 'videos/temp/temp.jpg'
    return path_to_upscaled_resized_image

def _add_blend_images(abi_path1, abi_path2):
    print("------add blend images -----------")
    print(abi_path1); print(abi_path2)
    abi_path2 = _resize_image(abi_path1, abi_path2)
    print(abi_path2)
    print(abi_path1)
    # Read Image1 
    abi_path1 = cv2.imread(abi_path1, 1) 
    
    # Read image2 
    abi_path2 = cv2.imread(abi_path2, 1) 
    
    # Blending the images with 0.3 and 0.7 
    img = cv2.addWeighted(abi_path1, 0, abi_path2, 1, 0) 

    abi_result_image_path = app.config['UPLOAD_FOLDER'] + 'videos/output/comb.jpg'
    cv2.imwrite(abi_result_image_path, img)

    return abi_result_image_path

def _downscale_resize_the_image(dri_path):

    print(os.getcwd())
    print(dri_path)
    img = cv2.imread(dri_path, cv2.IMREAD_UNCHANGED)    
    print('Original Dimensions : ',img.shape)
    
    scale_percent = 60 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    print("resized shape")
    print(resized.shape[0], resized.shape[1])

    cv2.imshow("Resized image", resized)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def _upscale_resize_the_image(uri_path):


    img = cv2.imread(uri_path, cv2.IMREAD_UNCHANGED)
 
    print('Original Dimensions : ',img.shape)
    
    scale_percent = 220 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    print(resized.shape[0], resized.shape[1])
    cv2.imshow("Resized image", resized)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def _resize_to_specific_size(rss_op_size , rss_path):
    '''
        returns size to specific ratio
    '''
 
    img = cv2.imread(rss_path, cv2.IMREAD_UNCHANGED)
    
    print('Original Dimensions : ',img.shape)
    
    width = rss_op_size[0]
    height = rss_op_size[1]
    dim = (width, height)
    
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    
    print('Resized Dimensions : ',resized.shape)
    
    # cv2.imshow("Resized image", resized)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    cv2.imwrite(rss_path, resized)
    return rss_path
# _downscale_resize_the_image('/home/hp/ML/NLP/text_summ/project/static/videos/output/comb.jpg')    
# _upscale_resize_the_image('/home/hp/ML/NLP/text_summ/project/static/videos/output/comb.jpg') 
# _resize_to_specific_size('/home/hp/ML/NLP/text_summ/project/static/videos/output/comb.jpg') 

def changeImageFormatToPng(pathto_input_image_folder):
    '''
    takes all the images in the folder and convert them to single format
    '''
    
    print("---------changeImageFormatToPng start")

    # pick all jpeg image convert to png
    images_jpeg = glob.glob(pathto_input_image_folder + '/' +"*.jpeg") 
    for file in images_jpeg:        
        im = Image.open(file)
        rgb_im = im.convert('RGB')
        
        rgb_im.save(file.replace("jpeg", "png"), quality=95)        
    
    # pick all jpg
    images_jpg = glob.glob(pathto_input_image_folder + '/' +"*.jpg")
    for file in images_jpg:        
        im = Image.open(file)
        rgb_im = im.convert('RGB')
        
        rgb_im.save(file.replace("jpg", "png"), quality=95)

    # pick all png images and transfer to output folder
    pathto_output_image_folder = app.config['UPLOAD_FOLDER'] + 'output'

    all_png_images = glob.glob(pathto_input_image_folder + '/' + "*.png")
    print("-----length of images to be transffered {0}".format(len(all_png_images)))
    for f in all_png_images:
        shutil.copy(f, pathto_output_image_folder)

    # convert all names to sequential file names
    path = pathto_output_image_folder
    files = os.listdir(path)
    print("-------number of files in the folder----- {0}".format(len(files)))
    for index,file in enumerate(files):
        print(index,file)
        
        os.rename(os.path.join(path, file), os.path.join(path, ''.join([str(index), '.png'])))

    return pathto_output_image_folder

# Function to change the image size
def changeImageSize(maxWidth, 
                    maxHeight, 
                    image):
    
    widthRatio  = maxWidth/image.size[0]
    heightRatio = maxHeight/image.size[1]

    newWidth    = int(widthRatio*image.size[0])
    newHeight   = int(heightRatio*image.size[1])

    newImage    = image.resize((newWidth, newHeight))
    return newImage    

def BlendImagesPillow(image1, image2): 
    '''
    '''
    print("blend image pillow ------")
    
    #     open images
    image1 = Image.open(image1)
    image2 = Image.open(image2)
    image3 = image1.convert("RGBA")
    image4 = image2.convert("RGBA")   

    alphaBlended1 = Image.blend(image3, image4, alpha=0.6)

    return alphaBlended1

def paste_Image_on_Image(pii_image_1_path, pii_image_2_path):
    '''
    '''
    print("------entered paster_image_on_image")
    print(pii_image_1_path); print(pii_image_2_path)
    
    image = Image.open(pii_image_1_path)
    logo = Image.open(pii_image_2_path)
    image_copy = image.copy()
    # position = ((image_copy.width - logo.width), (image_copy.height - logo.height))
    position = ((image_copy.width - logo.width), (0))  ## paste on top right
    image_copy.paste(logo, position, logo)
    # image_copy.save('pasted_image.jpg')

    return image_copy
