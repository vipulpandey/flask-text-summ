from flask import render_template, Blueprint
from flask import request, redirect, url_for, flash
from project import db
from project.models import Article
from .forms import AddArticleForm
import nltk
from nltk import tokenize
import random


def add_article_logic(form):


    # Get the input article text     
    article_text_input = form.article_content.data;
    print(type(article_text_input))
    # Number of sentences to be in the summary
    summary_sentence_len = form.final_sentence_count.data

    total_sentence_lists = tokenize.sent_tokenize(article_text_input)
    print("-------2-----------")
    print("total number of sentences in the article is: {}".format(len(total_sentence_lists)));

    
    article_text_output_list = []

    # check if length of text is less than total number of sentences, pick random number of sentences
    if (summary_sentence_len < len(total_sentence_lists)):  
        # for i in range(0, summary_sentence_len):
        #     # random numbe, summary_sentence_iven text
        #     # j = random.randint(0,len(total_sentence_lists) -1 )   
        print("----3-----")                                              
        print(summary_sentence_len)
        # random_index_list = random.sample(range(len(total_sentence_lists), summary_sentence_len))
        random_index_list = random.sample(range(len(total_sentence_lists)), summary_sentence_len)        
        # processing final output lists of sentences
        for j in random_index_list:
            article_text_output_list.append(total_sentence_lists[j])

    else:
        flash('Choose less number of sentences. There are {} sentences in the article. Select b/w 0-{}'.format(len(total_sentence_lists),len(total_sentence_lists)-1), 'success')
        print("Choose less number of sentences. There are {} sentences in the article. Select b/w 0-{}".format(len(total_sentence_lists),len(total_sentence_lists)-1))
    
    # article_text_output = "---";
    # article_text_output = article_text_output.join(article_text_output_list);
    i = 1;
    article_text_output = ""
    for sen in article_text_output_list:
        article_text_output = article_text_output + str(i) +" " +  sen + "\n"
        i = i + 1                            

    print("-----1-------------")
    print(type(article_text_output))
    # pushing data to database
    new_article = Article(form.final_sentence_count.data, form.article_content.data, article_text_output) 
    db.session.add(new_article)            
    db.session.commit()
    flash('New article, {}, added!'.format(new_article.final_sentence_count), 'success')

    return redirect(url_for('articles.index'))


