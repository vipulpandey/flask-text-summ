from flask import request, redirect, url_for, flash
from gensim.summarization.summarizer import summarize
from gensim.summarization import keywords
from project import db, app
import os 
import requests



def gensim_textrank_method(form):

    print("-------last--------")
       

    # # getting text document from Internet
    # text = requests.get('http://rare-technologies.com/the_matrix_synopsis.txt').text
        
    # # getting text document from file
    # fname = os.path.join(app.config['UPLOAD_FOLDER'], 'textfiles/test.txt')
    # with open(fname, 'r') as myfile:
    #     text=myfile.read()
    text = form.articletext.data  
    ratio = form.ratio.data

    #getting text document from web, below function based from 3
    from bs4 import BeautifulSoup
    from urllib.request import urlopen
    
    def get_only_text(url):
        """ 
        return the title and the text of the article
        at the specified url
        """
        page = urlopen(url)
        soup = BeautifulSoup(page, "lxml")
        text = ' '.join(map(lambda p: p.text, soup.find_all('p')))
        return soup.title.text, text    
    
    url = "https://www.kdnuggets.com/2019/01/solve-90-nlp-problems-step-by-step-guide.html"
    # text1 = get_only_text(url)        

    print('Summary:')
    print(summarize(text, ratio=ratio))
    summary = summarize(text, ratio=ratio)
    
    print ('\nKeywords:')
    kwords = keywords(text, ratio=ratio)
    
    
    
    print('\nKeywords:')
    
    # higher ratio => more keywords
    print (keywords(str(text), ratio=ratio))
    path_to_img_to_upload = "/static/ai_test.jpg"

    # path_to_img_to_upload = generate_media.generate_image_with_background(summary)  

    return (kwords, summary)