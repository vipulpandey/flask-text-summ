from PIL import Image, ImageDraw, ImageFont
from project import app
import textwrap 

# def text_wrap():
    
#     lines = textwrap.wrap(text, width=40)
#     y_text = h
#     for line in lines:
#         width, height = font.getsize(line)
#         draw.text(((w - width) / 2, y_text), line, font=font, fill=FOREGROUND)
#         y_text += height
def text_processing_info():
    '''
        input : summary of the text
        processing : gives nlp related info of the input text
        output : number of words in the given summary        
    '''  

    return None

    return num_words
def generate_image_without_background(summary):
    '''
        no background image has been provided

    '''
    text_ip = summary
    text_ip = "Happy diwali by DarpnAI !"

    # create font object with the font file and specify
    # desired size
    font_file_path = app.config['UPLOAD_FOLDER'] + 'fonts/Montserrat-Regular.ttf'

    # decide the font size
    font_size = 40
    font = ImageFont.truetype(font_file_path, size=font_size)

    # width, height of the background
    width, height = (200,30)
    img = Image.new('RGB', (width, height), color = (73, 109, 137))    

    d = ImageDraw.Draw(img)
    d.text((6,6), text_ip, fill=(255,255,0), font = font)
    
    
    # save the edited image
    image_output_name = 'image_output.png'
    image_output_path = app.config['UPLOAD_FOLDER'] + 'images/output/' + image_output_name
    img.save(image_output_path)
    return image_output_path

def generate_image_with_background(summary):

    '''
        images to be genetated  using Pillow to be posted to the social media--
        writing summary on the background image that has been provided
    '''
    # create Image object with the input image
    canvas_ip = app.config['UPLOAD_FOLDER'] + 'images/4.png'
    
    image = Image.open(canvas_ip)
    width, height = image.size
    # initialise the drawing context with
    # the image object as background
    draw = ImageDraw.Draw(image)

    # create font object with the font file and specify
    # desired size
    font_file_path = app.config['UPLOAD_FOLDER'] + 'fonts/Montserrat-Regular.ttf'

    # decide the font size
    font_size = 40
    font = ImageFont.truetype(font_file_path, size=font_size)
    
    # starting position of the summary
    (x, y) = (60, 60)

    # decides line length
    text_width = 40

    print("--------summary-------")
    print(summary)
    message = summary
    # color = 'rgb(0, 0, 0)' # black color
    text = summary
    # draw the message on the background
    
    # name = 'Python'
    # color = 'rgb(102, 0, 204)' 
    # color = 'rgb(255, 255, 153)' #light
    color = 'rgb(52, 235, 222)' #light
    # color = 'rgb(52, 31, 110)' #dark
    # draw.text((x, y), name, fill=color, font=font)
    print("------text wrap-----")
    lines = textwrap.wrap(text, width=text_width)
    
    for line in lines:
        draw.text((x, y), line, fill=color, font=font)
        y += 40;        

    
    print(lines)
    # save the edited image
    image_output_name = 'image_output.png'
    image_output_path = app.config['UPLOAD_FOLDER'] + 'images/output/' + image_output_name
    image.save(image_output_path)

    return image_output_path




    