from flask_wtf import Form, FlaskForm
from wtforms import StringField, IntegerField, TextField, DecimalField, TextAreaField
from wtforms.validators import DataRequired
 
 
class AddArticleForm(Form):
    
    final_sentence_count = IntegerField('Text Summarization length', validators=[DataRequired()])
    article_content = StringField('Article Content', validators=[DataRequired()])
    twitter_offset =  StringField('Twitter posting offset', validators=[DataRequired()])

class AddTextandArticle(Form):

    ratio = DecimalField('Ratio', validators=[DataRequired()])
    # date = StringField('Date', validators=[DataRequired()])
    articletext = TextAreaField('Article Content', validators=[DataRequired()])
    articleurl =  StringField('Twitter posting offset', validators=[DataRequired()])


class AddTextforCreative(Form):

    summaryText = TextAreaField('Article Summary Content', validators=[DataRequired("Please enter the summary")])
    twitterHashtags = TextAreaField('Hashtags for Twitter', validators=[DataRequired("Please enter the hashtags")])


class SumyForm(Form):
    
    articleurl =  StringField('Article URL')
    articletext = TextAreaField('Article Summary Content', validators=[DataRequired("Please enter the summary")])
    final_sentence_count = IntegerField('Text Summarization length', validators=[DataRequired()])
    

class AddVideoCreationForm(Form):
    video_title = TextAreaField('Video Title', validators=[DataRequired("Please enter title")])
    video_description = TextAreaField('Video Description', validators=[DataRequired("Please enter content")])