

# Load Packages
import sumy
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lex_rank import LexRankSummarizer





def textsumm_sumy_algorithm_method(form):

    

    

    document = form.articletext.data
    sentence =  form.final_sentence_count.data


    # For Strings
    parser = PlaintextParser.from_string(document,Tokenizer("english"))
    summarizer = LexRankSummarizer()
    print("-------sumy method----------")
    print(sentence)
    #Summarize the document with 2 sentences
    summary = summarizer(parser.document, sentence ) 
    
    added_sentences = " "
    for sentence in summary:
        print(type(str(sentence)))
        print(sentence)
        added_sentences = str(sentence) + added_sentences
    return added_sentences
