# imports

from flask import render_template, Blueprint, send_from_directory
from flask import request, redirect, url_for, flash
from werkzeug.utils import secure_filename
from project import db
from project.models import Article
from .forms import AddArticleForm, AddTextandArticle, AddTextforCreative, SumyForm, AddVideoCreationForm
import nltk
from nltk import tokenize
from datetime import datetime, date
import random, os
from PIL import Image
# import python files
from . import add_article_python
from . import text_summ__word_freq_algo, textsumm_gensim_textrank_algo
from . import textsumm_sumy_algorithm
from project import app
from . import post_to_social_media
from . import generate_media
from . import create_video
from . import create_video_crux_style

# congfig 
articles_blueprint = Blueprint('articles', __name__, template_folder = 'templates')
UPLOAD_FOLDER = '/path/to/the/uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # gives path till articles folder
images_directory = os.path.join(app.config['UPLOAD_FOLDER'], 'images')
thumbnails_directory = os.path.join(app.config['UPLOAD_FOLDER'], 'thumbnails')
if not os.path.isdir(images_directory):
    os.mkdir(images_directory)
if not os.path.isdir(thumbnails_directory):
    os.mkdir(thumbnails_directory)
    
############################################    

@articles_blueprint.route('/', methods=['GET', 'POST'])
def index(keywords = "empty"):
    
    return render_template('index.html', keywords = keywords)

@articles_blueprint.route('/check_db', methods = ['GET','POST'])
def get_data_db():
    all_articles = Article.query.all()
    print(Article.query.all())
    print("-------2-----------")
    
    return render_template('get_data_db.html', articles = all_articles)


@articles_blueprint.route('/live_site', methods = ['GET'])
def get_live_site():
    return render_template('statsmatter_live_page.html')


@articles_blueprint.route('/add/', methods = ['GET','POST'])    


#################### Random Sentence Picker ##############################

def random_sentence_picker_algo():       

    # form = AddArticleForm(int(request.form['final_sentence_count']),request.form['article_content']    

    if request.method == 'POST':
                
        form = AddArticleForm(request.form)
        print("---------add----------")
        print(request.form['final_sentence_count'])
        print(form.validate_on_submit())
        if form.validate_on_submit():                        
            add_article_python.add_article_logic(form)
            
        else:
            print("---------add else----------")
            flash('ERROR! Article was not added.', 'error')
    
    return render_template('add_article.html')



###################### For Sumy Lex Rank Algorithm #######################

@articles_blueprint.route('/sumy_lex_rank', methods = ['GET', 'POST'])

def sumy_lex_rank_method(keywords = "", summary = "" ):

    keywords = ""
    if request.method == 'POST':         

        #  form = SumyForm(request.form)
        print("-------sumy---------")
        print(request.form)
        keywords = ""
        # Validate the form
        form = SumyForm(request.form)
        print(form.final_sentence_count.data)
        if form.validate_on_submit():  

            summary = textsumm_sumy_algorithm.textsumm_sumy_algorithm_method(form)

            print(summary)
        if form.errors:
            for error_field, error_message in form.errors.items():
                flash("Field : {field}; error : {error}".format(field=error_field, error=error_message))
        
    else:
        print("problem with the post method")

    return render_template('index.html',keywords = keywords, summary = summary) 


############ For Word Frequency Algorithm ######################

@articles_blueprint.route('/add_word_freq', methods = ['GET','POST'])    
            
def nlp_word_freq():        
    if request.method == 'POST':
        # show generates output images

        form = AddArticleForm(request.form)
        print(request.form)
        print(type(request.form['article_content']))
        
        if form.validate_on_submit():                        
            path_to_img_to_upload = text_summ__word_freq_algo.word_freq_method(form)
            print("view ---------")
            path_to_img_to_upload =  "/static/images/output/image_output.png"
            print(path_to_img_to_upload)
        else:
            flash_errors(form)
            flash('ERROR! Article was not added.', 'error')
    
    else:   
        # show default background
        path_to_img_to_upload = "/static/images/darpnai_twitter_background.png"
    
    # path_to_img_to_upload = os.path.join(app.config['UPLOAD_FOLDER'], 'images/output/image_output.png')

    return render_template('nlp_word_freq_2.html', path_to_img_to_upload = path_to_img_to_upload )    


############### Gensim Algorithm ########################

@articles_blueprint.route('/add_genism', methods = ['GET','POST']) 

def text_summ__textrank_gensim():

    error = None
    # default data to be returned
    keywords = ""
    summary = ""
    path_to_img_to_upload = "/static/images/darpnai_twitter_background.png"

    if request.method == 'POST':

        form = AddTextandArticle(request.form)
        print(form)
        print("------Gensim testing-------")                
        
        if form.validate_on_submit():                        
            # path_to_img_to_upload = text_summ__word_freq_algo.word_freq_method(form)
            print("view ---------")
            try:
                #pushing the data to the database
                url = "https://www.statsmatter.in"
                heading =  "This is a demo heading" + str(form.ratio.data )
                content = form.articletext.data 

                # db data to push
                article = Article( date.today() ,datetime.now(),url,heading ,content, False)

                # db.session.add(article1)
                db.session.add(article)
                # commit the changes
                db.session.commit()
            except Exception as e:
                print(e)
                print("error adding to database")
                flash("Data not added to database")
            else:
                print("Successfully added to database") 
                flash("Successfully added to database")               

            keywords = ""
            (keywords, summary) = textsumm_gensim_textrank_algo.gensim_textrank_method(form)
            
            path_to_img_to_upload =  "/static/images/output/image_output.png"
            
        else:
            # 
            print("---Data not validated---")

    
    
    # path_to_img_to_upload = os.path.join(app.config['UPLOAD_FOLDER'], 'images/output/image_output.png')

    return render_template('index.html', keywords = keywords, summary = summary, error = error)

################ Posting creative to Social Media #################################

@articles_blueprint.route('/post_to_sm', methods = ['GET','POST'])   

def post_creative_to_social_media(path_to_img_to_upload = "/static/images/output/image_output.png"):

    '''
        picking generated image and posting it to the twitter and other social media
        input : path to input image
        output : nothing
    '''    
    path_to_generated_image = os.path.join(app.config['UPLOAD_FOLDER'], 'images/output/image_output.png')
    
    
    if request.method == 'POST':  
        print("---------request-----")  
        print(request.form)
        if request.form['post_sm'] == "yes_twitter":
            print(request.form['post_sm'])
            twitter_status = request.form['twitterstats']
            post_to_social_media.post_to_twitter(path_to_generated_image, twitter_status)
            print("-----post_to_creative-------")
            # image to be uploaded to html
            path_to_img_to_upload = "/static/images/output/image_output.png"
        elif request.form['post_sm'] == "yes_insta":
                  
            instagram_status = request.form['twitterstats']
            post_to_social_media.post_to_instagram(path_to_generated_image, instagram_status)
            # path_to_img_to_upload = "/static/images/darpnai_twitter_background.png"
            path_to_img_to_upload = "/static/images/output/image_output.png"
    else:
        redirect(url_for('articles.nlp_word_freq'))
    
    return render_template('result.html', path_to_img_to_upload = path_to_img_to_upload )

############## Final Result html #########################

@articles_blueprint.route('/result/', methods = ['GET','POST'])   

def result():
    # default value
    keywords = ""
    summary = ""
    if request.method == 'POST':        
        print("-----result-------")
        form = AddTextforCreative(request.form)
        summary = request.form['summaryText'] 
        keywords = request.form['twitterHashtags']
        path_to_img_to_upload = generate_media.generate_image_with_background(summary) 
        print(path_to_img_to_upload)
    else:
        print("-----no result-------")
    # keywords = request.form['summaryText'] 
    
    print(keywords)
    
    path_to_img_to_upload = "/static/images/output/image_output.jpg"
    return render_template('result.html',keywords = keywords,  path_to_img_to_upload = path_to_img_to_upload)


############## Get Video content #########################

@articles_blueprint.route('/get_video', methods = ['GET','POST'])   

def generate_video():        

    if request.method == 'POST': 

        print("---------generate video ------------")
        print("Generate Video Post Method is Working")
        form = AddVideoCreationForm(request.form)
        
        if form.validate_on_submit():
            print("Form is validated!")                        
            # create_video.create_video_auto_generated_images(form)
            # create_video.get_images_from_text(form)
            create_video_crux_style.get_video_style_first(form)
        else:
            flash("Form not validated!")  
            print("Form not validated!")            

    else: 
        print("generate video get method")

    return render_template('sm_video_content.html')


@articles_blueprint.route('/add_content', methods = ['GET','POST']) 

def generate_video_from_contents():
    print("-----generate_video_from_contents working")
    print(request.form)
    return redirect(url_for('articles.generate_video'))

@articles_blueprint.route('/upload', methods=['GET', 'POST'])     
def upload():

    if request.method == 'POST':
        print(" post is working")
        for upload in request.files.getlist('images'):
            filename = upload.filename
            
            # Always a good idea to secure a filename before storing it
            filename = secure_filename(filename)
            
            # This is to verify files are supported
            ext = os.path.splitext(filename)[1][1:].strip().lower()
           
            if ext in set(['jpg', 'jpeg', 'png']):
                print('File supported moving on...')
            else:
                return render_template('error.html', message='Uploaded files are not supported...')

            destination = '/'.join([images_directory, filename])
    
            # Save original image
            upload.save(destination)
            # Save a copy of the thumbnail image
            image = Image.open(destination)
            image.thumbnail((300, 170))            
            image.save('/'.join([thumbnails_directory, filename]))
        return redirect(url_for('articles.gallery'))

    return render_template('upload.html')

@articles_blueprint.route('/thumbnails/<filename>')
def thumbnails(filename):
    print(" ho ho ---------")
    print(app.config['UPLOAD_FOLDER'])
    return send_from_directory(app.config['UPLOAD_FOLDER'] + '/thumbnails', filename)

@articles_blueprint.route('/images/<filename>')
def images(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'] + '/images', filename)

@articles_blueprint.route('/gallery')     
def gallery():

    thumbnail_names = os.listdir(app.config['UPLOAD_FOLDER'] + "/thumbnails")   
    return render_template('gallery.html', thumbnail_names=thumbnail_names)