from flask import render_template, Blueprint
from flask import request, redirect, url_for, flash
from project import db, app
from project.models import Article
from .forms import AddArticleForm
import nltk
from nltk import tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize, sent_tokenize
import random
from . import generate_media

def word_freq_method(form):

    # Get the input article text     
    article_text_input = form.article_content.data;
    print("---@----")
    # print(article_text_input)
    # Number of sentences to be in the summary
    summary_sentence_len = form.final_sentence_count.data    
    
    # creating the word frequency table

    def _create_frequency_table(text_string) -> dict:

        stopWords = set(stopwords.words("english"))
        words = word_tokenize(text_string)
        ps = PorterStemmer()

        freqTable = dict()
        for word in words:
            word = ps.stem(word)
            if word in stopWords:
                continue
            if word in freqTable:
                freqTable[word] += 1
            else:
                freqTable[word] = 1

        return freqTable

    # scroing the sentences by the term frequency

    def _score_sentences(sentences, freqTable) -> dict:
        sentenceValue = dict()

        for sentence in sentences:
            word_count_in_sentence = (len(word_tokenize(sentence)))
            for wordValue in freqTable:
                if wordValue in sentence.lower():
                    if sentence[:10] in sentenceValue:
                        sentenceValue[sentence[:10]] += freqTable[wordValue]
                    else:
                        sentenceValue[sentence[:10]] = freqTable[wordValue]

            sentenceValue[sentence[:10]] = sentenceValue[sentence[:10]] // word_count_in_sentence

        return sentenceValue

    # find the threshold for sentence selelction

    def _find_average_score(sentenceValue) -> int:
        sumValues = 0
        for entry in sentenceValue:
            sumValues += sentenceValue[entry]

        # Average value of a sentence from original text
        average = int(sumValues / len(sentenceValue))

        return average

    # generate the summary

    def _generate_summary(sentences, sentenceValue, threshold):
        sentence_count = 0
        summary = ''

        for sentence in sentences:
            if sentence[:10] in sentenceValue and sentenceValue[sentence[:10]] > (threshold):
                summary += " " + sentence
                sentence_count += 1

        return summary


    # 1 Create the word frequency table
    freq_table = _create_frequency_table(article_text_input)

    '''
    We already have a sentence tokenizer, so we just need 
    to run the sent_tokenize() method to create the array of sentences.
    '''

    # 2 Tokenize the sentences
    sentences = sent_tokenize(article_text_input)

    # 3 Important Algorithm: score the sentences
    sentence_scores = _score_sentences(sentences, freq_table)

    # 4 Find the threshold
    threshold = _find_average_score(sentence_scores)

    # 5 Important Algorithm: Generate the summary
    summary = _generate_summary(sentences, sentence_scores, 1.5 * threshold)
    print("--------summary-------")
    print(summary)

    # # pushing data to database
    # new_article = Article(form.final_sentence_count.data, form.article_content.data, summary) 
    # db.session.add(new_article)            
    # db.session.commit()

    flash('New article, {}, added!'.format(new_article.final_sentence_count), 'success')

    #automatically posting to twitter 
    
    
    print(url_for('static', filename='ai_test.jpg'))
    if(form.twitter_offset.data == "yes"):
        path_to_img_to_upload = generate_media.generate_image_with_background(summary)                            
    else:
        # image to be uploaded by default
        path_to_img_to_upload = "/static/ai_test.jpg"

    return path_to_img_to_upload



