import numpy as np
import cv2
import glob
import re
from project import app
from . import graphic_image_manipulation
from . import graphic_text_manipulation

from PIL import Image, ImageDraw, ImageFont



def get_video_style_first(words_fit_inline_list, pts_text_design , pts_text_pos ):
    '''
        blends the input and output image creating a bit dark image sequence
    '''
    print("--------------------image_seq_with_background start")
    # pick all images of the folder 1) change to png 2) resize them to same size 3) Refines them
    input_image_folder = app.config['UPLOAD_FOLDER'] + 'images'

    output_image_folder = graphic_image_manipulation.changeImageFormatToPng(input_image_folder)
    print("----------output image")
    print(len(output_image_folder))

    # resize the images
    before_resize_images_files = glob.glob(output_image_folder + '/' +"*.png")
    print("----------before_resize_images_files",format(before_resize_images_files))
    
    # changeImageSize(1280, 720, image)
    OP_IMAGE_DIMENSION = (1280,720)
    for filename in glob.iglob(output_image_folder + '**/*.png', recursive=True):
        print(filename)
        im = Image.open(filename)
        imResize = im.resize(OP_IMAGE_DIMENSION, Image.ANTIALIAS)
        imResize.save(filename , 'png', quality=90)

    ######################### video creation process ###############
    slides_counter = 0
    
    BASE_IMAGE_SLIDE_PATH  = app.config['UPLOAD_FOLDER'] + 'output'  # png
    TEXT_SLIDE_PATH =  app.config['UPLOAD_FOLDER'] + 'videos/temp' # png    
    FINAL_OUTPUT_PATH = app.config['UPLOAD_FOLDER'] + 'videos/bg_temp'
    

    ## pick both images and blend
    before_blend_images_files = glob.glob(TEXT_SLIDE_PATH + '/' +"*.png")

    i = 0
    for f_name in range(len(before_blend_images_files)):
        print(f_name)
        _path1 = BASE_IMAGE_SLIDE_PATH + '/' + str(i) + '.png'
        _path2 = TEXT_SLIDE_PATH + '/' + str(i) + '.png'

        #     open images
        image1 = Image.open(_path1)
        image2 = Image.open(_path2)
        alphaBlended = Image.blend(image1, image2, alpha=0.6)
        print(FINAL_OUTPUT_PATH + "/" + str(i) + '.png')
        alphaBlended.save(FINAL_OUTPUT_PATH + "/" + str(i) + '.png')

        i = i + 1
    return True